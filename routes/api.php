<?php

use Illuminate\Http\Request;
use App\Http\Controllers\BrandsController;
use App\Http\Controllers\ModelsController;
use App\Http\Controllers\VehiclesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::prefix('v1')->group(function () {
    Route::get('brands', [BrandsController::class, 'index']);
    Route::get('models', [ModelsController::class, 'index']);
    Route::apiResource('vehicles', VehiclesController::class);
});