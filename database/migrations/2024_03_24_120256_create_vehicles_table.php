<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('model_id')->nullable(false);

            /*
                Х-ки год, пробег и цвет НАМЕРЕННО лежат в таблице "плоско"
                Это часть процесса денормализации, т.к. в современных UML
                моделях внешних сервисов данные х-ки выведены на уровень
                самого авто, а потому в данном месте допускается частичный 
                отказ от 1НФ (клеить с таблицами х-к в будующем - значительно
                более тяжелый процесс)
            */
            $table->integer('year')->nullable();
            $table->integer('mileage')->nullable();
            $table->string('color')->nullable();

            $table->timestamps();

            $table->foreign('model_id')->references('id')->on('models')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
};
