<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(5)->create();
        \App\Models\Brand::factory(10)->create();

        $brands = \App\Models\Brand::all();
        for ($i = 0; $i < 10; $i++)
            \App\Models\Model::factory(10)->for($brands->random())->create();

        $models = \App\Models\Model::all();
        for ($i = 0; $i < 100; $i++)
            \App\Models\Vehicle::factory(10)->for($models->random())->create();
    }
}
