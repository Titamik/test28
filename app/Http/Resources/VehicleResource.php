<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VehicleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,           
            // 'brand_id' => $this->brand_id,
            'model_id' => $this->model_id, 
            // 'brand_title' => $this->brand_title,
            // 'model_title' => $this->model_title,
            'year' => $this->year,
            'mileage' => $this->mileage,
            'color' => $this->color,
        ];
    }
}
