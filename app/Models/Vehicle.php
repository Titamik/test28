<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Model as EloquentModel;

use App\Models\Model;

class Vehicle extends EloquentModel
{
    use HasFactory;
    
    protected $fillable = [
        'model_id', 'year', 'mileage', 'color'
    ];
    
    public function model(): BelongsTo
    {
        return $this->belongsTo(Model::class);
    }
}
