<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Model as EloquentModel;

use App\Models\Model;

class Brand extends EloquentModel
{
    use HasFactory;

    public function models(): HasMany
    {
        return $this->hasMany(Model::class);
    }
}
